---
## So you made an app and nobody cares
![Cough](images/ethan.jpg)
Note:
Bigger hurdles for indie devs.  Work for XDA Developers.  Lead dev on their app store XDA Labs.  Have seen hundreds of devs.  Have seen what seems to work well.
---
### Keyz 2 Succe$$
- Design
- Promote
- Distribute
- Engage

Note:
How to design your app, Ways to promote your app, Ways to distribute your app, How to engage your users and create a community

---
### Design
---?image=images/praise.jpg&size=auto 90%
Material Design
- FAB, Toolbar, Navdrawer, Ripples
- What about the user experience?

Note:
MD is a guide line.  Other design considerations that might be skipped if app evolved from prototype.
---
### Get a designer
- A designer can put together a big picture flow for your app.  They can create user stories, mock-ups and make sure your app is following proper guidelines.
- They can be the difference between a fancy prototype and a well thought out, enjoyable app.
---
### Graphix
A designer can help create your assets with you.
- Icons
- Banner image
- Screenshots
---
### Where do I find a designer?
- Design/UX Meetups |
- Reddit |
- Play store |
- Material up |
- Dribbble |

Note:
You could even give a talk at a meetup about the struggles of design in Android as a dev.
Sub-reddits for designers.  Themes and icon packs in the play store are by designers.  Material up and Dribbble let you browse designs you may like and directly contact indie designers.
---
![Hangar](images/hangar_feature.jpg)

Note:
Even without a designer care should be taken.
---
## Promote
![ShitTogether](/images/shit_together.gif)

Note:
Good looking app = Better first impression = Sites showcasing it
---
### Press release 
- Banner/Icon
- Lead to grab the reader
- Description, Features, Changes
- Presskit

Note:
Sites need content.  You can sell your app in a 1-2 pager.  Presskit has images, video preview,etc
---
### Get the word out

- Blog (XDA, Engadget, Mobile Syrup, Phandroid, Android Authority, Lifehacker)
- Podcasts, YouTube

Note:
Snowball effect.  XDA features, then Lifehacker sees it and features.

---
## Distribute
Note:
We all know play, but there are other stores that have communities around them
---
### Pockets of users
![Labs](images/labs.png) ~1000 apps
![FDroid](images/fdroid.png) ~2500 apps

Note:
Big fish, small pond.  Depending on app, 10-15% of users from these other platforms
---
## Engage
Note:
Leverage your fans and the community
---
### Engage and foster a community
> "Many users, given the chance, will help you" - Jeff Corcoran
Note:
Give users an avenue to help or contribute.  Always reassure when you can, provide calls to action if something goes wrong or you need their help
---?image=images/feed_network_error.png&size=auto 90%
---?image=images/custom_activity_on_crash.jpg&size=auto 90%
---?image=images/labs_crash_page.png&size=auto 90%
---?image=images/feed_translators.png&size=auto 90%
### CrowdIn and beta testers
Note:
Give credit to translators in your app
---
### Keyz 2 Succe$$
- Design (Make your app pretty with a designer)
- Promote (Send out a press release!)
- Distribute (Big fish, small pond)
- Engage (Become a chat slave!)
